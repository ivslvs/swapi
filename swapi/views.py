from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.shortcuts import redirect
from django.core.files.base import ContentFile
from .models import CsvFile
import requests
import uuid
import csv
import pandas


SWAPI_PEOPLE = 'https://swapi.dev/api/people/'


class CollectionListView(ListView):

    model = CsvFile
    template_name = "collections.html"
    context_object_name = 'collections'


class CollectionDetailView(DetailView):

    model = CsvFile
    template_name = "collection.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        file = pandas.read_csv(f'media/uploads/{self.object.filename}')
        result = {}
        i = 1
        for index, row in file.iterrows():
            result.update({i: row.to_dict()})
            i += 1
        context['collections'] = result
        return context


def get_collection(request):
    filename = f'{uuid.uuid4()}.csv'
    file = CsvFile.objects.create(filename=filename)
    file.upload.save(filename, ContentFile(''))

    response = requests.get(SWAPI_PEOPLE).json()

    with open(f'media/uploads/{filename}', mode='w') as file:

        data_writer = csv.writer(file)
        column = [['name', 'height', 'mass', 'hair_color', 'skin_color', 'eye_color', 'birth_year',
                   'gender', 'homeworld', 'date']]
        data_writer.writerows(column)

        for row in response['results']:
            homeworld = requests.get(row['homeworld']).json()

            data_writer.writerow(
                [row['name'], row['height'], row['mass'], row['hair_color'], row['skin_color'], row['eye_color'],
                 row['birth_year'],
                 row['gender'], homeworld['name'], row['edited'][:10]])

    return redirect('collections:list')
