from django.db import models


class CsvFile(models.Model):
    """CSV file model"""

    filename = models.CharField(max_length=100)
    upload = models.FileField(upload_to='uploads/', blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-creation_date']

    def __str__(self):
        return f'{self.creation_date} - {self.filename}'
