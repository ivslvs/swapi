from django.urls import path
from .views import get_collection, CollectionListView, CollectionDetailView

app_name = "collections"


urlpatterns = [
    path('upload/collection/', get_collection, name='upload'),
    path('collections/', CollectionListView.as_view(), name='list'),
    path('collections/<int:pk>/', CollectionDetailView.as_view(), name="detail"),

]
